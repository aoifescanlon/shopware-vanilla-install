(function ($) {

    $.overridePlugin('swProductSlider', {

        slide: function(){
            var me = this;
            me.customMethod;
            me.superclass.slide.apply(this, arguments)
        },
        customMethod: function () {
            var tick = new Date().toLocaleTimeString();
            console.info('Slide!' + tick);
        }
    });

})(jQuery);