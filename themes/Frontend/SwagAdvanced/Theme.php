<?php

namespace Shopware\Themes\SwagAdvanced;

use Doctrine\Common\Collections\ArrayCollection;
use Shopware\Components\Form;
use Shopware\Components\Theme\ConfigSet;

class Theme extends \Shopware\Components\Theme
{
    protected $extend = 'Responsive';
    protected $name = 'Swag Advanced Theme';
    protected $description = '';
    protected $author = '';
    protected $license = '';
    protected $inheritanceConfig = false;

    protected $javascript = [
        'src/js/jquery.cart-animation.js',
        'src/js/jquery.slider-demo.js',
        'src/js/jquery.scroll-nav.js'
    ];

    private $sets = [
        'green' => ['name' => 'Green set', 'desc' => 'Everything is green',
            'values' => [
                'backgroundColor' => '#EDF6F4',
                'brand-primary' => '#00A392'
            ]
        ],
        'purple' => ['name' => 'Purple set', 'desc' => 'Everything is purple',
            'values' => [
                'backgroundColor' => '#F4EDF6',
                'brand-primary' => '#84139C'
            ]
        ],
    ];

    public function createConfig(Form\Container\TabContainer $container)
    {
//        first video:
//        $tab = $this->createTab('first_tab','My First Tab', []);
//        $fieldset = $this->createFieldSet('first_fieldset', 'My Fieldset', []);
//        $textField = $this->createTextField('first_fieldset', 'My first textfield', 'Some Text', []);
//
//        $fieldset->addElement($textField);
//        $tab->addElement($fieldset);
//        $container->addTab($tab);

//        second advanced template video:
        $tabPanel = $this->createTabPanel('main_tab_panel');

        $tabPanel->addTab($this->createColorTab());
        $tabPanel->addTab($this->createSettingsTab());

        $tab = $this->createTab(
          'main_tab',
          '__workshop_tab_name__'
        );

        $tab->addElement($tabPanel);
        $container->addElement($tab);
    }

    /**
     * @return Form\Container\Tab
     */
    private function createColorTab()
    {
        $tab = $this->createTab(
          'color_tab',
          'Colors'
        );

        $fieldset = $this->createFieldSet(
          'color_fieldset',
          'Colors'
        );

        $backgroundColor = $this->createColorPickerField(
            'backgroundColor',
            'background color',
            '#EEEEEE'

        );

        $primaryColor = $this->createColorPickerField(
            'brand-primary',
            'primary color',
            '#990000'

        );

        $fieldset->addElement($backgroundColor);
        $fieldset->addElement($primaryColor);
        $tab->addElement($fieldset);

        return $tab;
    }

    private function createSettingsTab()
    {
        $sloganField = $this->createTextField(
            'themeSlogan',
            '__slogan_field_label__',
            '',
            ['attributes' => [
                'lessCompatible' => false,
                'anchor' => '100%'
            ]]
        );

        $scrollNavField = $this->createCheckboxField(
          'scrollNav',
          '__scroll_nav_field_label__',
          false
        );

        $scrollNavDisplayPositionField = $this->createNumberField(
            'scrollNavDisplayPosition',
            '__scroll_nav_display_field_label__',
            300
        );

        $settingsFieldset = $this->createFieldSet(
            'settings_fieldset',
            '__settings_fieldset_label__'
        );

        $settingsFieldset->addElement($sloganField);
        $settingsFieldset->addElement($scrollNavField);
        $settingsFieldset->addElement($scrollNavDisplayPositionField);

        $tab = $this->createTab(
            'settings_tab',
            '__settings_tab_label__'
        );

        $tab->addElement($settingsFieldset);

        return $tab;
    }

    public function createConfigSets(ArrayCollection $collection)
    {

        foreach ($this->sets as $set) {
            $set = $this->createConfigSet($set);
            $collection->add($set);
        }
    }

    public function createConfigSet($s)
    {
        $set = new ConfigSet();
        $set->setName($s['name']);
        $set->setDescription($s['desc']);
        $set->setValues($s['values']);
        return $set;
    }

//    green
//    background = #EDF6F4
//    primary = #00A392

//    purple
//    background = #F4EDF6
//    primary = #84139C

}