<?php

namespace SwagStartup\Services;

use Doctrine\DBAL\Connection;

class ProductNameService
{
    /** @var Connection*/
    private $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getProductNames()
    {
        $queryBuilder = $this->connection->createQueryBuilder();

        $result = $queryBuilder->select(['name'])
            ->from('s_articles')
            ->setMaxResults(20)
            ->execute();

        return $result->fetchAll(\PDO::FETCH_COLUMN);
    }
}