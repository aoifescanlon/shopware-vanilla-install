{extends file="parent:frontend/index/index.tpl"}

{block name="frontend_index_sidebar"}

{/block}

{block name='frontend_index_content'}

    <h1>Hi, I'm {$currentAction}-Action</h1>
    <a href="{url module='frontend' controller='routing_demonstration' action=$nextPage}">
        {*{s name='RoutingDemonstrationNextPage'}Next Page{/s}*}
        Next Page
    </a>
    <ul>
        {foreach $productNames as $productName}
            <li>{$productName}</li>
        {/foreach}
    </ul>
{/block}