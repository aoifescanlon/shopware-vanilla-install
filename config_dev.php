<?php
return [
    'db' => [
        'username' => 'root',
        'password' => 'root',
        'dbname' => 'shopware',
        'host' => 'localhost',
        'port' => '3306'
    ],

    'csrfProtection' => [
        'frontend' => false,
        'backend' => false
    ],

    'phpsettings' => [
        'error_reporting' => E_ALL & ~E_USER_DEPRECATED,
        'display_errors' => 1,
        'date.timezone' => 'Europe/Dublin',
    ],
    'snippet' => [
        'readFromDb' => true,
        'writeToDb' => true,
        'readFromIni' => false,
        'writeToIni' => false,
        'showSnippetPlaceholder' => true,
    ],

    'front' => [
        'noErrorHandler' => true,
        'throwExceptions' => true,
        'showException' => true
    ],
    'httpcache' => [
        'enabled' => true,
        'lookup_optimization' => true,
        'debug' => false,
        'default_ttl' => 0,
        'private_headers' => ['Authorization', 'Cookie'],
        'allow_reload' => false,
        'allow_revalidate' => false,
        'stale_while_revalidate' => 2,
        'stale_if_error' => false,
        'cache_dir' => $this->getCacheDir() . '/html',
        'cache_cookies' => ['shop', 'currency', 'x-cache-context-hash'],
    ],

    'trustedproxies' => ['127.0.0.1'],

    'template' => [
        'forceCompile' => true
    ],

    'session' => [
        'locking' => false
    ],

];